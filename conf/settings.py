# Settings for Alfred

# Connection settings
server = 'irc.shadow.agency'  # Server hostname/ip
port = 6697  # Port to connect to
channels = ("#lab",)  # Channel to connect to. Only one channel can be used currently
nick = 'maeve'  # Name the bot should use

# Admin for the bot (Beware security risks on networks without service/daemon secured nicknames!)
owner = 'vaetis'

# Name of log file to use. You can include a path here as well.
logfile = 'maeve.log'

# Modules
module_list = (
    'example',   # Example module framework for custom plugins
    )

# Enable/Disable SSL
ssl_enable = True