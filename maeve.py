#!/usr/bin/python -B

import socket, os, ssl, sys, datetime, imp, random
from time import sleep
from conf import settings

## Default Command List
# Note: With the exception of the Core Module, individual module commands are not included here as they are intrinsically separate from the
# main script.
commands = (
	'!commands',
    '!shutdown',
    )

## Import Settings
server = settings.server
channels = settings.channels
port = settings.port
nick = settings.nick
owner = settings.owner
module_list = settings.module_list
ssl_enabled = settings.ssl_enable

## Debug Mode
debug = 1

## Functions

# Process input through loaded modules.
def check_modules(nick, user, hostmask, channel, content):
	for module in module_list:
		loaded_module = imp.load_source(module, './modules/' + module + '.py')
		getreply = getattr(loaded_module, 'return_data')
		output = getreply(speak, nick, user, hostmask, channel, content)
		if output != content and output != None:
			speak(channel, output)

# Send regular keep-alive pings so the server doesn't kill the connection as a time-out.
def ping():
	ircsock.send('PONG :ping\n')
	if debug == 1:
		print 'Keep-alive Ping sent.'

# Process output through here. Some modules may bypass this method local to themselves using the ircsock global.
def speak(channel, msg):
	output = []
	msg = str(msg)
	if debug == 1:
		print msg

	if len(msg) > 450:
		for i in range(0, len(msg), 450):
			output.append(msg[i:i + 450])
		for message_chunk in output:
			ircsock.send('PRIVMSG ' + channel + ' :' + message_chunk + '\n')
			sleep(2)
			if debug == 1:
				print message_chunk
	else:
		ircsock.send('PRIVMSG ' + channel + ' :' + msg + '\n')
		if debug == 1:
			print msg

# Join supplied list of channels from settings file.
def joinchan(ircsock, channels):
	for channel in channels:
		ircsock.send('JOIN ' + channel + '\n')

# Clean-up raw IRC input before handling it.
def process_message(irc_rawdata):
	info_list = irc_rawdata.split(' ')
	user_host = info_list[0]
	user_host = user_host.split('!')
	user = user_host[0].replace(':', '')
	host = user_host[1].split('@')
	host = host[1]
	channel = info_list[2]
	content = irc_rawdata.split(' :')
	content = str(content[1])
	return (user, host, channel, content)

# A quick and easy way to check for core bot commands.
def command_check(user, channel, content):
	for command in commands:
		if command in content and user == owner and content == '!shutdown':
			speak(channel, 'Shutting down...')
			quit()


# Function for initiating a connection and providing a socket to send and recieve info.
# This creates a global socket which may allow a module to send directly and bypass all bot
# functions, and should be more cleanly implemented.
def connect():
	try:
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect((server, port))
	except:
		print 'Unable to connect.'
		quit()
	if ssl_enabled == True:
		ircsock = ssl.wrap_socket(sock)
	ircsock.send('USER ' + nick + ' ' + nick + ' ' + nick + ' :Tagline!\n')
	ircsock.send('NICK ' + nick + '\n')
	joinchan(ircsock, channels)
	return ircsock

# Main bot loop.
def main():
	pid = os.fork()
	if pid > 0:
		sys.exit(0)
	os.setsid()
	print 'Loaded. Connecting to ' + server + ' and becoming a daemon...'
	global ircsock
	ircsock = connect()
	while True:
		irc_rawdata = ircsock.recv(1024)  # Set a buffer for data obtained by the server
		if debug == 1:
			print irc_rawdata
		irc_rawdata = irc_rawdata.strip('\n\r')  # Clear unwanted linebreaks in the raw data
		if 'PING :' in irc_rawdata:
			ping()
		elif 'PRIVMSG' in irc_rawdata:
			data = process_message(irc_rawdata)
			if debug == 1:
				print data
			user = data[0]
			host = data[1]
			channel = data[2]
			content = data[3]
			command_check(user, channel, content)
			check_modules(nick, user, host, channel, content)

# Run the script.
if __name__ == "__main__":
	main()
