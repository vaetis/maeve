#!/usr/bin/python -B

## Modules
import time, os, sys, random, json, string

## Settings
loop = True
name = 'maeve'

subjects = [
    'who is', 'who are', 'who were', 'who was',
    'what is', 'what are', 'what were', 'what was',
    'when is', 'when are', 'when were', 'when was',
    'where is', 'where are', 'where were', 'where was',
    'why is', 'why are', 'why were', 'why was'
    ]

greetings = [
    'Hello',
    'Hi there',
    'Hi',
    'Hey',
    'Hey there',
    ]

confirmations = [
    'Ok',
    'Understood',
    'Alright',
    'I understand',
    ]

## Functions
def speech_processor(data):
    data = str(data)
    for match in greetings:
        if data.lower() == match.lower(): 
            print random.choice(greetings)

def fact_check(data):
    for item in subjects:
        if item.lower() in data.lower():
            fact_type = data.split(' ')
            fact_type = fact_type[1]
            if fact_type == 'is':
                keywords = data.lower().split(item + ' ', 1)
                keywords[1] = keywords[1].translate(None, string.punctuation)
                if keywords[1] in facts_is:
                    begin = keywords[1].split(' ')
                    begin = begin[0]
                    print begin.title() + ' is ' + facts_is[keywords[1]] + '.'
                    print '\n'
                else:
                    print 'I\'m not sure.\n'
            if fact_type == 'are':
                keywords = data.lower().split(item + ' ', 1)
                keywords[1] = keywords[1].translate(None, string.punctuation)
                if keywords[1] in facts_are:
                    begin = keywords[1].split(' ')
                    begin = begin[0]
                    print begin.title() + ' are ' + facts_are[keywords[1]] + '.'
                    print '\n'
                else:
                    print 'I\'m not sure.\n'
            if fact_type == 'were':
                keywords = data.lower().split(item + ' ', 1)
                keywords[1] = keywords[1].translate(None, string.punctuation)
                if keywords[1] in facts_were:
                    begin = keywords[1].split(' ')
                    begin = begin[0]
                    print begin.title() + ' were ' + facts_were[keywords[1]] + '.'
                    print '\n'
                else:
                    print 'I\'m not sure.\n'
            if fact_type == 'was':
                keywords = data.lower().split(item + ' ', 1)
                keywords[1] = keywords[1].translate(None, string.punctuation)
                if keywords[1] in facts_was:
                    begin = keywords[1].split(' ')
                    begin = begin[0]
                    print begin.title() + ' was ' + facts_was[keywords[1]] + '.'
                    print '\n'
                else:
                    print 'I\'m not sure.\n'
            return True

def record_fact(data):
    choices = ('But I thought', 'But as I understood it,', 'I thought', 'I was led to believe')
    data = data.replace(' I ', ' you ')
    data = data.replace(' i ', ' you ')
    data = data.replace('We ', 'You ')
    data = data.replace(' we ', ' you ')
    data = data.replace('I am ', 'you are ')
    data = data.replace('I\'m ', 'you\'re ')
    if ' is ' in data:
        fact = data.lower().split(' is ', 1)
        if fact[0] not in facts_is:
            facts_is[fact[0]] = fact[1].translate(None, string.punctuation)
            print 'Ok. I\'ll remember that ' + fact[0].lower() + ' is ' + fact[1].lower()
        else:
            print '%s %s is %s?' % (random.choice(choices), fact[0], facts_is[fact[0]])
        data = 'none'
    elif ' are ' in data:
        fact = data.lower().split(' are ', 1)
        if fact[0] not in facts_are:
            facts_are[fact[0]] = fact[1].translate(None, string.punctuation)
            print 'Ok. I\'ll remember that ' + fact[0].lower() + ' is ' + fact[1].lower()
        else:
            print '%s %s is %s?' % (random.choice(choices), fact[0], facts_are[fact[0]])
    elif ' were ' in data:
        fact = data.lower().split(' were ', 1)
        if fact[0] not in facts_were:
            facts_were[fact[0]] = fact[1].translate(None, string.punctuation)
            print 'Ok. I\'ll remember that ' + fact[0].lower() + ' were ' + fact[1].lower()
        else:
            print '%s %s is %s?' % (random.choice(choices), fact[0], facts_were[fact[0]])
    elif ' was ' in data:
        fact = data.lower().split(' was ', 1)
        if fact[0] not in facts_was:
            facts_was[fact[0]] = fact[1].translate(None, string.punctuation)
            print 'Ok. I\'ll remember that ' + fact[0].lower() + ' was ' + fact[1].lower()
        else:
            print '%s %s is %s?' % (random.choice(choices), fact[0], facts_was[fact[0]])

def help_text():
    print "Set a fact by providing a key followed by is, are, were, was, then the content."
    print "Example: This is a test will record \'this\' being set to \'a test\'."
    print ""
    print "Get a fact by referencing who, what, when, where and why, followed by is, are, were or was, then the key."
    print "Example: \'What is this\' would return \'a test\' based on the above example."
    print ""
    print "Type \'quit\' to exit and save all facts."

## Main
facts_are = json.load(open('facts_are.db'))
facts_were = json.load(open('facts_were.db'))
facts_was = json.load(open('facts_was.db'))
facts_is = json.load(open('facts_is.db'))

os.system('clear')
print "MAEVE - MAchine EVolving Entity"
while loop == True:
    data = raw_input('> ')
    speech_processor(data)
    if fact_check(data) != True:
        record_fact(data)
    if data.lower() == 'help' or data.lower() == 'commands':
        help_text()
    if data.lower() == 'quit':
        loop = False
        json.dump(facts_is, open("facts_is.db", 'w'), indent=4)
        json.dump(facts_are, open("facts_are.db", 'w'), indent=4)
        json.dump(facts_were, open("facts_were.db", 'w'), indent=4)
        json.dump(facts_was, open("facts_was.db", 'w'), indent=4)
        print "Quitting..."
