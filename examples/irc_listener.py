#!/usr/bin/python

import socket, ssl, os, sys, random, string, re, time

# Bot settings
server = 'irc.shadow.agency'
port = 6697
channels = ("#lab","#freeipa")
nick = 'friday'

# Functions
def ping():
	ircsock.send('PONG :ping\n')
	
def speak(channel, msg):
	ircsock.send('PRIVMSG '+ channel +' :'+ msg +'\n')
	print 'RESPONSE: ' + msg

def joinchan(channels):
	for channel in channels:
		ircsock.send('JOIN '+ channel +'\n')

def hello(channel):
	ircsock.send('PRIVMSG '+ channel +' :Hello!\n')

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((server, port))
ircsock = ssl.wrap_socket(sock)
ircsock.send('USER '+ nick +' '+ nick +' '+ nick +' :Tagline!\n')
ircsock.send('NICK '+ nick +'\n')
joinchan(channels)

while True:
	irc_rawdata = ircsock.recv(2048)
	irc_rawdata = irc_rawdata.strip('\n\r') # Clear unwanted linebreaks in the raw data
	if "PING :" in irc_rawdata and nick not in irc_rawdata:
		ping()
	else:
		print irc_rawdata